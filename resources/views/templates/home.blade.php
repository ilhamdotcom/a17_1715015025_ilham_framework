<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <!-- Bootstrap core CSS -->
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <link href="{{asset('css/dashboard.css')}}" rel="stylesheet">
        @yield('css')
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
            <a class="navbar-brand px-4" href="{{ route('home') }}">Book's Shoop</a>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                    <div class="sidebar-sticky">
                        <ul class="nav flex-column">
                            <li class="nav-item ">
                                <a class="nav-link active" href="{{ route('users.index') }}">
                                    <span data-feather="users"></span>
                                    Users
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link active" href="{{ route('categories.index') }}">
                                    <span data-feather="list"></span>
                                    Categories
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link active" href="{{ route('books.index') }}">
                                    <span data-feather="book"></span>
                                    Books
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link active" href="{{ route('orders.index') }}">
                                    <span data-feather="clipboard"></span>
                                    Orders
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    @yield('content')
                </main>
            </div>
        </div>
        <footer class="footer">
            <div class="container text-center text-light">
                <span>&copy;ilham_dot_com 2019</span>
            </div>
        </footer>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="{{asset('js/app.js')}}"></script>
        <!-- Icons -->
        <script src="{{asset('js/feather.min.js')}}"></script>
        <script> feather.replace() </script>
        @yield('js')
    </body>
</html>