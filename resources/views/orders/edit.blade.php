@extends('templates.home')
@section('title')
    Edit Order
@endsection
@section('content')
    <div class="container" >
        <h3>Form Edit Order</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5>{{ $order['invoice_number'] }}</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('orders.update',$order['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">                        
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-3">
                                <label for="total" class="text-primary">Tottal Price</label>
                            </div>
                            <div class="col-md-8">
                                <input type="number" class="form-control {{$errors->first('total_price') ? "is-invalid": ""}}" name="total_price" id="total_price" value="{{ old('total_price') ? old('total_price') : $order['total_price'] }}">                                 
                                <div class="invalid-feedback">                        
                                    {{$errors->first('total_price')}}              
                                </div>               
                            </div>
                        </div>
                        <br>
                        <div class="row">                             
                            <div class="col-md-3">                                 
                                <label for="user_id">User</label>                   
                            </div>                             
                            <div class="col-md-8">             
                                <select name="user_id" id="user_id" class="form-control {{$errors->first('user_id') ? "is-invalid": ""}}">                    
                                    <option value="">Select User</option>
                                    @foreach ($users as $user)          
                                        <option value="{{ $user->id }}" {{ $order->user_id==$user->id ? "selected":'' }}>{{ $user->username }}</option>            
                                    @endforeach                                 
                                </select>                                 
                                <div class="invalid-feedback">            
                                    {{$errors->first('user_id')}}        
                                </div>                             
                            </div>                         
                        </div>                         
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="status" class="text-primary">Status</label>
                            </div>
                            <div class="col-md-8">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input {{ $order['status'] == "SUBMIT" ? 'checked':'' }}
                                        type="radio" id="submit" name="status" class="custom-control-input" value="SUBMIT">
                                    <label class="custom-control-label" for="submit">SUBMIT</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input {{ $order['status'] == "PROCESS" ? 'checked':'' }}
                                        type="radio" id="process" name="status" class="custom-control-input" value="PROCESS">
                                    <label class="custom-control-label" for="process">PROCESS</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input {{ $order['status'] == "FINISH" ? 'checked':'' }}
                                        type="radio" id="finish" name="status" class="custom-control-input" value="FINISH">
                                    <label class="custom-control-label" for="finish">FINISH</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input {{ $order['status'] == "CANCEL" ? 'checked':'' }}
                                        type="radio" id="cancel" name="status" class="custom-control-input" value="CANCEL">
                                    <label class="custom-control-label" for="cancel">CANCEL</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary" >Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection