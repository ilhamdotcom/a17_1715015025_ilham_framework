@extends('templates.home')
@section('title')
    Detail Order
@endsection
@section('content')
    <h1>Detail Order </h1>
    <hr>
    <br>
    <div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:200px;">
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>{{ $order['invoice_number'] }}</h3>
            </div>
        </div>
        <hr>
        <br>
        <div class="row"> 
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">             
                User           
            </div> 
            <div class="col-md-4 col-sm-4">             
                {{ $order->user->username }}
            </div>                 
            <br>             
        </div>  
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                Total Price
            </div>
            <div class="col-md-4 col-sm-4 ">
                {{ $order['total_price'] }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                Status
            </div>
            <div class="col-md-4 col-sm-4 ">
                @if ( $order['status'] == "SUBMIT" )
                    <span class="badge badge-warning""> {{ $order['status'] }} </span>
                @elseif ( $order['status'] == "PROCESS" )
                    <span class="badge badge-primary""> {{ $order['status'] }} </span>
                @elseif ( $order['status'] == "FINISH" )
                    <span class="badge badge-success""> {{ $order['status'] }} </span>
                @else 
                    <span class="badge badge-danger""> {{ $order['status'] }} </span>
                @endif
            </div>
            <br>
        </div>
    </div>
@endsection