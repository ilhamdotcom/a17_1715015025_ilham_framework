@extends('templates.home')
@section('title')
    Order List
@endsection
@section('css')
    <style>
        body{
            padding-top: 30px;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        td a{
            margin: 3px;
            align-content: center;
            color: white;
        }
        td a:hover{
            text-decoration: none;
        }
        td button{
            margin-top: 5px;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <h3>Order List</h3><hr>
        <div class="row">
            <div class="col-md-4">
                <a class="btn btn-outline-primary " href="{{ route('orders.create') }}">
                    <span data-feather="plus-circle"></span>
                    Add<span class="sr-only">(current)</span>
                </a>
            </div>
            <div class="col-md-7">
                <form action=" {{ route('orders.search') }} " method="get">
                    <div class="input-group custom-search-form">
                        <input type="text" name="keyword" placeholder="Search with Invoice Number" class="form-control" id="search">
                        <select name="keyword2" class="col-sm-2">
                            <option value="">
                                ALL
                            </option>
                            <option value="SUBMIT">
                                SUBMIT
                            </option>
                            <option value="PROCESS">
                                PROCESS
                            </option>
                            <option value="FINISH">
                                FINISH
                            </option>
                            <option value="CANCEL">
                                CANCEL
                            </option>
                        </select>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">
                                <span data-feather="search"></span> Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <br>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr class="table-primary">
                        <th scope="col">ID</th>
                        <th scope="col">Invoice Number</th>
                        <th scope="col">Total Price</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <td>{{ $order['id'] }}</td>
                            <td>{{ $order['invoice_number'] }}</td>
                            <td>{{ $order['total_price'] }}</td>
                            <td>
                                @if ( $order['status'] == "SUBMIT" )
                                    <span class="badge badge-primary""> {{ $order['status'] }} </span>
                                @elseif ( $order['status'] == "PROCESS" )
                                    <span class="badge badge-warning""> {{ $order['status'] }} </span>
                                @elseif ( $order['status'] == "FINISH" )
                                    <span class="badge badge-success""> {{ $order['status'] }} </span>
                                @else 
                                    <span class="badge badge-danger""> {{ $order['status'] }} </span>
                                @endif
                            </td>
                            <td>
                                <a class="btn-sm btn-primary" href="{{ route('orders.show',$order['id']) }}">
                                <span data-feather="eye"></span>
                                Detail <span class="sr-only">(current)</span></a>
                                <a class="btn-sm btn-success d-inline" href="{{ route('orders.edit',$order['id']) }}">
                                <span data-feather="edit-2"></span>
                                Edit <span class="sr-only">(current)</span></a>
                                <form class="d-inline"
                                    onsubmit="return confirm('Delete this order permanently?')"
                                    action="{{route('orders.destroy', $order['id'])}}"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                    <span data-feather="trash"></span>
                                    Delete <span class="sr-only">(current)</span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection