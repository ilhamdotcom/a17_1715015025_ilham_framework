@extends('templates.home')
@section('title')
    Edit Category
@endsection
@section('content')
    <div class="container" >
        <h3>Form Edit Category</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5>{{ $category['category_name'] }}</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('categories.update',$category['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">                        
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-3">
                                <label for="description" class="text-primary">Description</label>
                            </div>
                            <div class="col-md-8">
                                <textarea name="description" class="form-control {{$errors->first('description') ? "is-invalid": ""}}" id="description" cols="20" rows="5">{{ old('description') ? old('description') : $category['description'] }}</textarea>                  
                                <div class="invalid-feedback">   
                                    {{$errors->first('description')}}                 
                                </div> 
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary" >Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection