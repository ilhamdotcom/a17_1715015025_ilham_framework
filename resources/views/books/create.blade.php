@extends('templates.home')
@section('title')
    Create Book
@endsection
@section('content')
    <div class="container" >
        <h3>Create Book</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">     
                <strong>{{ session('status') }}</strong>  
                <button type="button" class="close" data-dismiss="alert" arialabel="Close">       
                    <span aria-hidden="true">&times;</span> 
                </button>             
            </div>         
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5> Create a New Book</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('books.store') }}" class="form-group" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="title" >Title</label>
                            </div>
                            <div class="col-md-8">
                            <input value="{{ old('title') }}" type="text" class="form-control {{ $errors->first('title')? "is-invalid": "" }}" name="title" id="title">        
                                <div class="invalid-feedback">   
                                    {{$errors->first('title')}} 
                                </div>
                            </div>     
                        </div>
                        <br>
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="author">Author</label>
                            </div>
                            <div class="col-md-8">
                                <input value="{{ old('author') }}" type="text" class="form-control {{ $errors->first('author')? "is-invalid": "" }}" name="author" id="author">        
                                <div class="invalid-feedback">   
                                    {{$errors->first('author')}} 
                                </div>
                            </div>     
                        </div>
                        <br>
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="publisher">Publisher</label>
                            </div>
                            <div class="col-md-8">
                                <input value="{{ old('publisher') }}" type="text" class="form-control {{ $errors->first('publisher')? "is-invalid": "" }}" name="publisher" id="publisher">        
                                <div class="invalid-feedback">   
                                    {{$errors->first('publisher')}} 
                                </div>
                            </div>     
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="description">Description</label>
                            </div>
                            <div class="col-md-8">
                                <textarea name="description" class="form-control {{$errors->first('description') ? "is-invalid": ""}}" id="description" cols="20" rows="5">{{ old('description') }}</textarea>
                                <div class="invalid-feedback">              
                                    {{$errors->first('description')}}          
                                </div>                            
                            </div> 
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="price">Price</label>
                            </div>
                            <div class="col-md-8">
                                <input value="{{ old('price') }}" type="number" class="form-control {{ $errors->first('price')? "is-invalid": "" }}" name="price" id="price">        
                                <div class="invalid-feedback">   
                                    {{$errors->first('price')}} 
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="stock">Stock</label>
                            </div>
                            <div class="col-md-8">
                                <input value="{{ old('stock') }}" type="number" class="form-control {{ $errors->first('stock')? "is-invalid": "" }}" name="stock" id="stock">        
                                <div class="invalid-feedback">   
                                    {{$errors->first('stock')}} 
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="category">Category</label>
                            </div>
                            <div class="col-md-8">    
                                <select multiple='multiple' name="category[]" id="category" class="form-control {{$errors->first('category') ? "is-invalid": ""}}">        
                                    <option value="">Select Category</option>           
                                    @foreach ($categories as $category)    
                                        <option value="{{ $category->id }}">{{ $category->category_name }}</option> 
                                    @endforeach  
                                </select>
                                <div class="invalid-feedback">
                                    {{$errors->first('category')}}   
                                </div>                 
                            </div>    
                        </div>
                        <br>
                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="col-md-3 text-primary">
                                    Cover
                                </div>
                                <div class="col-md-8">
                                    <div class="custom-file">        
                                        <label for="cover" class="custom-file-label">Cover</label>              
                                        <input type="file" class="custom-file-input {{$errors->first('cover') ? "is-invalid": ""}}" name="cover" id="cover">                  
                                        <div class="invalid-feedback">             
                                            {{$errors->first('cover')}}           
                                        </div>                                 
                                    </div>      
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection