@extends('templates.home')
@section('title')
    Edit Book
@endsection
@section('content')
    <div class="container" >
        <h3>Form Edit Book</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5>{{ $book['title'] }}</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('books.update',$book['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">                        
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-3">
                                <label for="author" class="text-primary">Author</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control {{$errors->first('author') ? "is-invalid": ""}}" name="author" id="author" value="{{ old('author') ? old('author') : $book['author'] }}">                                 
                                <div class="invalid-feedback">                        
                                    {{$errors->first('author')}}              
                                </div>   
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="publisher" class="text-primary">Publisher</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control {{$errors->first('publisher') ? "is-invalid": ""}}" name="publisher" id="publisher" value="{{ old('publisher') ? old('publisher') : $book['publisher'] }}">                                 
                                <div class="invalid-feedback">                        
                                    {{$errors->first('publisher')}}              
                                </div>   
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="description" class="text-primary">Description</label>
                            </div>
                            <div class="col-md-8">
                                <textarea name="description" class="form-control {{$errors->first('description') ? "is-invalid": ""}}" id="description" cols="20" rows="5">{{ old('description') ? old('description') : $book['description'] }}</textarea>                  
                                <div class="invalid-feedback">   
                                    {{$errors->first('description')}}                 
                                </div>
                            </div> 
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="price" class="text-primary">Price</label>
                            </div>
                            <div class="col-md-8">
                                <input type="number" class="form-control {{$errors->first('price') ? "is-invalid": ""}}" name="price" id="price" value="{{ old('price') ? old('price') : $book['price'] }}">                                 
                                <div class="invalid-feedback">                        
                                    {{$errors->first('price')}}              
                                </div>   
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="stock" class="text-primary">Stock</label>
                            </div>
                            <div class="col-md-8">
                                <input type="number" class="form-control {{$errors->first('stock') ? "is-invalid": ""}}" name="stock" id="stock" value="{{ old('stock') ? old('stock') : $book['stock'] }}">                                 
                                <div class="invalid-feedback">                        
                                    {{$errors->first('stock')}}              
                                </div>   
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="category">Category</label>
                            </div>
                            <div class="col-md-8">    
                                @foreach($book->category as $category)       
                                    <span class="badge badge-primary">{{ $category->category_name }}  </span> 
                                @endforeach 
                                <select multiple='multiple' name="category[]" id="category" class="form-control {{$errors->first('category') ? "is-invalid": ""}}">        
                                    <option value="">Select a New Category</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->category_name }}</option> 
                                    @endforeach
                                    </select>     
                                <div class="invalid-feedback">
                                    {{$errors->first('category')}}   
                                </div>        
                            </div>   
                        </div>
                        <br>
                        <div class="row">     
                            <div class="col-md-3">                    
                                Cover                             
                            </div>                             
                            <div class="col-md-8">              
                                <img src="{{asset('storage/'.$book['cover'])}}" class="img-thumbnail" height="150px" width="150px" alt="">                
                                <div class="custom-file">                              
                                    <label for="cover" class="custom-file-label">Cover</label>         
                                    <input type="file" class="custom-file-input {{$errors->first('cover') ? "is-invalid": ""}}"  name="cover" id="cover">                 
                                    <div class="invalid-feedback">                 
                                        {{$errors->first('cover')}}               
                                    </div>                                 
                                </div>                             
                            </div>                         
                        </div>                         
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary" >Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection