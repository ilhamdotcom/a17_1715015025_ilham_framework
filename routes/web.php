<?php

Route::get('/', function () {
    return view('templates/home');
})->name('home');

Route::resource('/users', 'UserController');
Route::get('/user', 'UserController@search')->name('users.search');

Route::resource('/categories', 'CategoryController');
Route::get('/category', 'CategoryController@search')->name('categories.search');

Route::resource('/books', 'BookController');
Route::get('/book', 'BookController@search')->name('books.search');

Route::resource('/orders', 'OrderController');
Route::get('/order', 'OrderController@search')->name('orders.search');