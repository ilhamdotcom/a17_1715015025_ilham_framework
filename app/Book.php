<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'description', 'author', 'publisher', 'price', 'stock'
    ];

    public function category()     
    {         
        return $this->belongsToMany(Category::class);
    }
}
