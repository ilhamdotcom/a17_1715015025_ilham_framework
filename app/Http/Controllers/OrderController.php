<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Order; 
use App\User; 

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::paginate(5);
        return view('orders.index',compact('orders'));
    }

    public function create()
    {
        $users = User::all();         
        return view('orders.create',['users'=>$users]); 
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id'=>'required',             
            'total_price'=>'required',             
            'invoice_number'=>'required|min:8|unique:orders',             
            'status'=> 'required',  
        ]); 

        Order::create([
            'user_id'=>$request->user_id,             
            'total_price'=>$request->total_price,             
            'invoice_number'=>$request->invoice_number,             
            'status'=>$request->status, 
        ]);    
        return redirect()->route('orders.create')->with('status','Order Succesfully Created');     
    } 

    public function show($id)
    {
        $order = Order::findOrFail($id);         
        return view('orders.show',['order'=>$order]); 
    }

    public function edit($id)
    {
        $order = Order::findOrFail($id); 
        $users = User::all();         
        return view('orders.edit',['order'=>$order,'users'=>$users]); 
    }

    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id); 
        $request->validate([
            'user_id'=>'required',             
            'total_price'=>'required',                
            'status'=> 'required',  
        ]); 
        $update_order = Order::findOrFail($id);         
        $update_order->user_id = $request->get('user_id');
        $update_order->total_price = $request->get('total_price');         
        $update_order->status = $request->get('status');          
        $update_order->save();   
        return redirect()->route('orders.edit',['order'=>$order])->with('status','Order Successfully Updated'); 
    }

    public function destroy($id)
    {
        $order = Order::findOrFail($id);  
        $order->delete();         
        return redirect()->route('orders.index')->with('status','Order Successfully Deleted'); 
    }

    public function search(Request $request)
    {
        $orders = Order::when($request->keyword, function ($query) use ($request) {
            $query->where('status', 'like', "%{$request->keyword2}%")
                ->where('invoice_number', 'like', "%{$request->keyword}%");
        })->paginate(5);

        $orders->appends($request->only('keyword'));

        return view('orders.index', compact('orders'));
    }
}
