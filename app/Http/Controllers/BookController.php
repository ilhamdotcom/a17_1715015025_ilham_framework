<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Book;
use App\Category;  

class BookController extends Controller
{
    public function index()
    {
        $books = Book::paginate(5);
        return view('books.index',compact('books'));
    }

    public function create()
    {
        $categories =Category::all();
        return view('books.create',['categories'=>$categories]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|max:100|unique:books',             
            'description'=>'required|max:200',             
            'author'=>'required',                   
            'publisher'=>'required',       
            'price'=> 'required',            
            'stock'=> 'required',  
            'category'=> 'required',
            'cover'=>'required|mimes:jpg,jpeg,png,bmp', 
        ]); 

        $new_book = new Book();         
        $new_book->title = $request->get('title');
        $new_book->description = $request->get('description');         
        $new_book->author = $request->get('author');         
        $new_book->publisher = $request->get('publisher');         
        $new_book->price = $request->get('price'); 
        $new_book->stock = $request->get('stock');         
        if ($request->file('cover')) {             
            $file = $request->file('cover')->store('covers','public');
            $new_book->cover = $file;         
        }         
        $new_book->save(); 
        
        $new_book->category()->attach($request->get('category'));

        return redirect()->route('books.create')->with('status','Book Succesfully Created');     
    }

    public function show($id)
    {
        $book = Book::findOrFail($id); 
        return view('books.show',['book'=>$book]); 
    }

    public function edit($id)
    {
        $categories =Category::all();
        $book = Book::findOrFail($id);  
        return view('books.edit',['book'=>$book,'categories'=>$categories]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([           
            'description'=>'required|max:200',             
            'author'=>'required',                    
            'publisher'=>'required',         
            'price'=> 'required',            
            'stock'=> 'required',  
            'category'=> 'required',
        ]); 
        $update_book = Book::findOrFail($id);         
        $update_book->description = $request->get('description');         
        $update_book->author = $request->get('author');        
        $update_book->publisher = $request->get('publisher');        
        $update_book->price = $request->get('price'); 
        $update_book->stock = $request->get('stock');
        if ($request->file('cover')) {             
            if ($update_book->cover && file_exists(storage_path('app/public/'.$update_book->cover))) {                 
                Storage::delete('public/'.$update_book->cover);             
            }             
            $file = $request->file('cover')->store('covers','public');
            $update_book->cover = $file;         
        }         
        $update_book->save();      
        
        $update_book->category()->sync($request->get('category'));

        return redirect()->route('books.edit',['id'=>$id])->with('status','Book succesfully updated');     
    
    }

    public function destroy($id)
    {
        $book = Book::findOrFail($id);        
        if ($book->cover && file_exists(storage_path('app/public/'.$book->cover))) {             
            Storage::delete('public/'.$book->cover);         
        }         
        $book->delete();        
        return redirect()->route('books.index')->with('status', 'Book Successfully deleted');      
    }

    public function search(Request $request)
    {
        $books = Book::when($request->keyword, function ($query) use ($request) {
            $query->where('title', 'like', "%{$request->keyword}%");
        })->paginate(5);

        $books->appends($request->only('keyword'));

        return view('books.index', compact('books'));
    }
}
