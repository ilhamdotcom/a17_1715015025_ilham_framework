<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Category; 

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::paginate(5);
        return view('categories.index',compact('categories'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_name'=>'required|min:4|max:100|unique:categories',             
            'description'=>'required|max:200',                  
        ]); 

        Category::create([             
            'category_name'=>$request->category_name,             
            'description'=>$request->description,     
        ]);

        return redirect()->route('categories.create')->with('status','Category Successfully Created');
    }

    public function show($id)
    {
        $category = Category::findOrFail($id);         
        return view('categories.show',['category'=>$category]); 
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);       
        return view('Categories.edit',['category'=>$category]); 
    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id); 
        $request->validate([                         
            'description'=>'required|max:200',                     
        ]);  
        $update_category = Category::findOrFail($id);         
        $update_category->description = $request->get('description');
        $update_category->save();         
        return redirect()->route('categories.edit',['category'=>$category])->with('status','Category Successfully Updated'); 
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);  
        $category->delete();         
        return redirect()->route('categories.index')->with('status','Category Successfully Deleted'); 
    }

    public function search(Request $request)
    {
        $categories = Category::when($request->keyword, function ($query) use ($request) {
            $query->where('category_name', 'like', "%{$request->keyword}%");
        })->paginate(5);

        $categories->appends($request->only('keyword'));

        return view('categories.index', compact('categories'));
    }
}
