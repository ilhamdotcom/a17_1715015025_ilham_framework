<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB; 

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            ['title'=>"Jejak Langkah",'price'=>50000,'author'=>"Pramoedya Ananta Toer",'publisher'=>"PERS",'description'=>"Perjuangan kaum terpelajar pribumi mulai berhasil mendirikan organisasi modern dan berhasil berkembang pesat",'stock'=>30,'cover'=>''],
            ['title'=>"The World Until Yesterday",'price'=>70000,'author'=>"Jared Diamond",'publisher'=>"PERS",'description'=>"Pengetahuan mengenai kehidupan masyarakat tradisional tahap pemburu peramu/peladang yang masih ada sampai abad 20 dari seluruh belahan dunia",'stock'=>30,'cover'=>''],
            ['title'=>"The Selfish Gene",'price'=>80000,'author'=>"Richard Dawkins",'publisher'=>"PERS",'description'=>"Penjelasan fenomena sosial sebagai ekosistem serta diri sebagai individu & organisme dari sudut pandang gen yang ada dalam tubuh",'stock'=>30,'cover'=>''],
            ['title'=>"Laskar Pelangi",'price'=>60000,'author'=>"Andrea Hirata",'publisher'=>"PERS",'description'=>"Novel ini bercerita tentang kehidupan 10 anak dari keluarga miskin yang bersekolah di sekolah Muhammmadiyah di Belitung yang penuh dengan keterbatasan",'stock'=>30,'cover'=>''],
            ['title'=>"THE 100",'price'=>60000,'author'=>"Michael H. Hart",'publisher'=>"PERS",'description'=>"Memuat 100 tokoh yang memiliki pengaruh paling besar dan paling kuat dalam sejarah manusia",'stock'=>30,'cover'=>''],
            ['title'=>"Bumi Manusia",'price'=>50000,'author'=>"Pramoedya Ananta Toer",'publisher'=>"PERS",'description'=>"Perjuangan kaum terpelajar pribumi mulai berhasil mendirikan organisasi modern dan berhasil berkembang pesat",'stock'=>30,'cover'=>''],
            ['title'=>"The World Until Yesterday",'price'=>70000,'author'=>"Jared Diamond",'publisher'=>"PERS",'description'=>"Pengetahuan mengenai kehidupan masyarakat tradisional tahap pemburu peramu/peladang yang masih ada sampai abad 20 dari seluruh belahan dunia",'stock'=>30,'cover'=>''],
            ['title'=>"The Selfish Gene",'price'=>80000,'author'=>"Richard Dawkins",'publisher'=>"PERS",'description'=>"Penjelasan fenomena sosial sebagai ekosistem serta diri sebagai individu & organisme dari sudut pandang gen yang ada dalam tubuh",'stock'=>30,'cover'=>''],
            ['title'=>"Laskar Pelangi",'price'=>60000,'author'=>"Andrea Hirata",'publisher'=>"PERS",'description'=>"Novel ini bercerita tentang kehidupan 10 anak dari keluarga miskin yang bersekolah di sekolah Muhammmadiyah di Belitung yang penuh dengan keterbatasan",'stock'=>30,'cover'=>''],
            ['title'=>"THE 100",'price'=>60000,'author'=>"Michael H. Hart",'publisher'=>"PERS",'description'=>"Memuat 100 tokoh yang memiliki pengaruh paling besar dan paling kuat dalam sejarah manusia",'stock'=>30,'cover'=>''],
            ['title'=>"Jejak Langkah",'price'=>50000,'author'=>"Pramoedya Ananta Toer",'publisher'=>"PERS",'description'=>"Perjuangan kaum terpelajar pribumi mulai berhasil mendirikan organisasi modern dan berhasil berkembang pesat",'stock'=>30,'cover'=>''],
            ['title'=>"The World Until Yesterday",'price'=>70000,'author'=>"Jared Diamond",'publisher'=>"PERS",'description'=>"Pengetahuan mengenai kehidupan masyarakat tradisional tahap pemburu peramu/peladang yang masih ada sampai abad 20 dari seluruh belahan dunia",'stock'=>30,'cover'=>''],
            ['title'=>"The Selfish Gene",'price'=>80000,'author'=>"Richard Dawkins",'publisher'=>"PERS",'description'=>"Penjelasan fenomena sosial sebagai ekosistem serta diri sebagai individu & organisme dari sudut pandang gen yang ada dalam tubuh",'stock'=>30,'cover'=>''],
            ['title'=>"Laskar Pelangi",'price'=>60000,'author'=>"Andrea Hirata",'publisher'=>"PERS",'description'=>"Novel ini bercerita tentang kehidupan 10 anak dari keluarga miskin yang bersekolah di sekolah Muhammmadiyah di Belitung yang penuh dengan keterbatasan",'stock'=>30,'cover'=>''],
            ['title'=>"THE 100",'price'=>60000,'author'=>"Michael H. Hart",'publisher'=>"PERS",'description'=>"Memuat 100 tokoh yang memiliki pengaruh paling besar dan paling kuat dalam sejarah manusia",'stock'=>30,'cover'=>''],
    ]);
    }
}
