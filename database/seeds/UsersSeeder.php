<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB; 

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'username'=>"Ilham",'email'=>"ilhamdotcom81@gmail.com",
                'address'=>"Bima",'phone'=>"081234567890",
                'status'=>'ACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>"Pujo",'email'=>"pujodotcom81@gmail.com",
                'address'=>"Samarinda",'phone'=>"081234567891",
                'status'=>'INACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>"Uxdiin",'email'=>"uxdiindotcom81@gmail.com",
                'address'=>"Berau",'phone'=>"081234567892",
                'status'=>'ACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>"Notwo",'email'=>"notwodotcom81@gmail.com",
                'address'=>"Balikpapan",'phone'=>"081234567893",
                'status'=>'ACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>"Hamle",'email'=>"hamledotcom81@gmail.com",
                'address'=>"Dompu",'phone'=>"081234567894",
                'status'=>'INACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'indah','email'=>'indah@gmail.com',
                'address'=>'jl pramuka indah','phone'=>'081234567890',
                'status'=>'ACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'rani','email'=>'rani@gmail.com',
                'address'=>'jl perjuangan rani','phone'=>'081234567891',
                'status'=>'INACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'jihyo','email'=>'jihyo@gmail.com',
                'address'=>'jl gelatik jihyo','phone'=>'081234567892',
                'status'=>'ACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'yerin','email'=>'yerin@gmail.com',
                'address'=>'jl pemuda yerin','phone'=>'081234567893',
                'status'=>'ACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'nat','email'=>'nat@gmail.com',
                'address'=>'jl alam segar nat','phone'=>'081234567894',
                'status'=>'INACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'hana','email'=>'hana@gmail.com',
                'address'=>'jl pramuka hana','phone'=>'081234567895',
                'status'=>'ACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'hani','email'=>'hani@gmail.com',
                'address'=>'jl perjuangan hani','phone'=>'081234567896',
                'status'=>'INACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'hina','email'=>'hina@gmail.com',
                'address'=>'jl gelatik hina','phone'=>'081234567897',
                'status'=>'ACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'nana','email'=>'nana@gmail.com',
                'address'=>'jl pemuda nana','phone'=>'081234567898',
                'status'=>'ACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
            [
                'username'=>'nani','email'=>'nani@gmail.com',
                'address'=>'jl alam segar nani','phone'=>'081234567899',
                'status'=>'INACTIVE','avatar'=>'','password'=>Hash::make("123456")
            ],
        ]);
    }
}
