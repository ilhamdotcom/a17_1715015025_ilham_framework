<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB; 

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['category_name'=>"Sejarah",'description'=>"Buku tentang Sejarah"],
            ['category_name'=>"Sosial",'description'=>"Buku tentang Kehidupan Sosial"],
            ['category_name'=>"Sains",'description'=>"Buku tentang Ilmu Pengetahuan"],
            ['category_name'=>"Novel",'description'=>"Buku yang berisi cerita fiksi"],
            ['category_name'=>"Biografi",'description'=>"Buku tentang Biografi(datadiri & perjalanana hidup) seseorang yang memiliki jasa"],
            ['category_name'=>"Aksi",'description'=>"Berisi aksi-aksi"],
            ['category_name'=>"Budaya",'description'=>"Buku yang menjelaskan Budaya tertentu"],
            ['category_name'=>"Sci-Fi",'description'=>"Buku yang berisi sebuah karangan yang berdasarkan ilmu pengetahuan"],
            ['category_name'=>"Ekonomi",'description'=>"Buku yang berisi tentang ilmu-ilmu Ekonomi"],
            ['category_name'=>"Teknologi",'description'=>"Buku yang berisi perkembangan teknologi"],
            ['category_name'=>"Komik",'description'=>"Buku cerita bergambar"],
            ['category_name'=>"Antropologi",'description'=>"Buku yang membahas tentang ilmu perbintangan"],
            ['category_name'=>"Kedirgantaraan",'description'=>"Buku yang membahas tentang penerbangan"],
            ['category_name'=>"Agama",'description'=>"Buku yang membahas tentang Agama"],
            ['category_name'=>"Kelautan",'description'=>"Buku yang membahas ilmu kelautan"],
        ]);
    }
}
