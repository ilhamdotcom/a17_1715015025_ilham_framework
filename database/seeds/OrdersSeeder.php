<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB; 

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('orders')->insert([
            ['user_id'=>1,'total_price'=>20000,'invoice_number'=>'43256561','status'=>"PROCESS"],
            ['user_id'=>2,'total_price'=>36570,'invoice_number'=>'53464632','status'=>"CANCEL"],
            ['user_id'=>3,'total_price'=>13900,'invoice_number'=>'35346253','status'=>"FINISH"],
            ['user_id'=>4,'total_price'=>19800,'invoice_number'=>'54363744','status'=>"SUBMIT"],
            ['user_id'=>5,'total_price'=>30000,'invoice_number'=>'12343425','status'=>"PROCESS"],
            ['user_id'=>6,'total_price'=>24770,'invoice_number'=>'43256566','status'=>"PROCESS"],
            ['user_id'=>7,'total_price'=>38000,'invoice_number'=>'53464637','status'=>"CANCEL"],
            ['user_id'=>8,'total_price'=>14000,'invoice_number'=>'35346258','status'=>"FINISH"],
            ['user_id'=>9,'total_price'=>79000,'invoice_number'=>'54363749','status'=>"SUBMIT"],
            ['user_id'=>10,'total_price'=>35000,'invoice_number'=>'12343410','status'=>"PROCESS"],
            ['user_id'=>11,'total_price'=>24000,'invoice_number'=>'43256511','status'=>"PROCESS"],
            ['user_id'=>12,'total_price'=>34000,'invoice_number'=>'53464612','status'=>"CANCEL"],
            ['user_id'=>13,'total_price'=>15000,'invoice_number'=>'35346213','status'=>"FINISH"],
            ['user_id'=>14,'total_price'=>79000,'invoice_number'=>'54363714','status'=>"SUBMIT"],
            ['user_id'=>15,'total_price'=>35000,'invoice_number'=>'12343415','status'=>"PROCESS"],
        ]);
    }
}
