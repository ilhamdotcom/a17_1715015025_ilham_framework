<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(OrdersSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(BooksSeeder::class);
        $this->call(Book_CategorySeeder::class);
    }
}
